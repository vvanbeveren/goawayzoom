# Go Away Zoom

This is a script / application which closes Zoom for Windows when you press the close button on the Zoom window, or use ALT+F4.

Zoom no longer has the option to automatically close zoom when it is closed. In this state it still takes up valueble system resources while not being useful. This tiny application fixes that (though keeping the application in memory also uses a bit of resources).

## Usage

Download and compile the script using autohotkey V2. Or simply download a precompiled executable from here.

See:
 https://www.autohotkey.com/


## Adding to Windows on start-up

[Download](https://surfdrive.surf.nl/files/index.php/s/1EaKOsZG3f5hCVJ) the executable, or compile the script using autohotkey V2.

Press Windows+R to open the "Run" dialog box.
Type "shell:startup" and then hit Enter to open the "Startup" folder.
Drag the app from the download folder, or make a shortcut to it.

[![CC0](https://i.creativecommons.org/p/zero/1.0/88x31.png "CC0")](http://creativecommons.org/publicdomain/zero/1.0/)
