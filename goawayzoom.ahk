; Autohotkey V2 script for closing zoom, when closed by pressing close button
; or ALT+F4
;
; Written by Vincent van Beveren
;
; Comes without any guarantees

#HotIf WinActive("ahk_class ZPPTMainFrmWndClassEx")
~LButton::
{
    CoordMode "Mouse", "Screen"
    MouseGetPos &X, &Y, &HWND
    if !(HWND = WinExist("Zoom Meeting")) {
		try
		{
			Result := SendMessage(0x84,, (Y << 16) | X,, HWND)
			if (Result = 20)
				ProcessClose "zoom.exe"
		} catch TimeoutError as err {
		}	
    }
    return
}

#HotIf WinActive("ahk_class ZPPTMainFrmWndClassEx")
$!F4::
{
	ProcessClose "zoom.exe"
	return
}
